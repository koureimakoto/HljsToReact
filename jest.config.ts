/* eslint-env node, es6 */

export default {
    reporters: [
        "default",
        //"<rootDir>/src/lib/test/Over.cjs"
    ],
    clearMocks: true,
    coverageProvider: "v8",
     moduleFileExtensions: [
       "js",
       "ts",
       "tsx",
       "json",
     ],
    setupFilesAfterEnv: [
      '<rootDir>/src/lib/test/setup.ts'
    ],
    testEnvironment: "jsdom",
    transform: {
        "^.+\\.(t|j)sx?$": [
            "@swc/jest", {
                jsc: {
                    parser: {
                        syntax: 'typescript',
                        tsx: true,
                        decorators: true
                    },
                    keepClassNames: true,
                    transform: {
                        legacyDecorator: true,
                        decoratorMetadata: true,
                        react: {
                            runtime: 'automatic'
                        }
                    }
                },
                module: {
                    type: 'es6',
                    noInterop: false
                }
            }
        ]
    },
};
  