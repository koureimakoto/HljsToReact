## Seja bem vindo à Highlight.JS to React.JS ! ( It is less unstable. )

### Update Note

**v.: 0.0.7**  
> ^ Use only for testing, it is still unstable.

  
 **v.: 0.0.9 < Current**
> Fixed de Readme Instrctions.

> If the chosen programming languages is different from the code. The probability of return *error* is higher. Warning.

> All line break become a Div tag, now. The Div receive a enumerated style for animation. --l: $number

> You have the option to enumerate the lines in the HTML. <  ... options={e_num: true, hover: true}  >


**v.: 0.1.0 - Alpha**
> You will be able to copy the line with one-click mouse event.

> The option to change css on Javascript

> Added line mouse hover option.
---

Thank you for considering using this tool. It is, although still unstable, particularly functional. There are still several features that I still need to implement to improve comfort and security, and it is a more reliable option than injecting code with innerHTML or dangerouslySetInnerHTML.

Read more about [**XSS**](https://owasp.org/www-community/attacks/xss/)

>Any questions, suggestions, or bugs found, please contact us to get it resolved as soon as possible.


## How to use it


```jsx

import  HljsToReact  from  "hljstoreact"
import  'hljstoreact/style'

//Brute Mode
<HljsToReact  
	code="export function \n fn(){}" 
	lang="typescript" 
	options={{e_num: true, hover: false}}
/>

//Recommend Mode
const  input = {
code:`
export function
fn() {
	return true
}
`, lang:  "typescript",
	options={{e_num: true, hover: false}}
}
---

<HljsToReact> code={input.code} lang={input.lang} options={input.options}/>
> return /* JSX.Element[] */(
	<div>
		<pre  className="...">
			<code>
				<div styele="--line: ${count}" className="...">
					<div>{lineEnum}</div>
					<div>{lineText}</div>
				</div>
				...
			<code>
		</pre>
	</div>
)

```
>The **HLJS**  lib will return HTML code that will be compiled for  **React**.
