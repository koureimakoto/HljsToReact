import { SpanNode, SpanParseNode, TagNode, TokenList, UnwrappedContent } from './DataTypes'

/**
 *  **Span Parser Queue**
 * 
 * This class  represents the  data struct for Abstract Syntax Queue.  Carefully 
 * observe I did write Tree,  so read the Span Parser Class comments to know how
 * to use it correctly.
 * 
 * ```typescript
 * 
 * CLASS
 *   private token              : number               // TokenList number
 *   private count              : number               // Children in queueu
 *   private child              : SpanParseNode | null // Next Child
 *   private last_removed_child : SpanParseNode | null // Temporary Buffer
 *   private last_child_ptr     : function // with memory reference  
 * 
 * ```
 */
export default class
AbstractSyntaxTree {
    private level
    private count = 0
    private child              : TagNode | SpanNode | UnwrappedContent | null
    // eslint-disable-next-line
    private last_removed_child : any
    private last_child_ptr = () => this.child
    
    constructor( level: number ) {
        this.level = level
        this.child = null
    }

    checkLevel(): boolean {
        if( this.level <= 0 )
            return false
        return true
    }

    upLevel() {
        return this.level++
    }

    downLevel() {
        return this.level--
    }



    /**
     *  Return the last child removed with remove() function
     * ```typescript
     * 
     * getLastRemoved
     *   << last_removed_child: SpanParseNode
     * 
     * ```
     */
    getLastRemoved(): SpanParseNode {
        // eslint-disable-next-line 
        return this.last_removed_child
    }
    
    /**
     * Return the number of nodes in the queue
     *```typescript
     * 
     * getNodeCount
     *   +  verbose
     *   << count: number 
     * 
     * ```
     */
    getNodeCount(): number {
        console.log( 'Node count: ' + this.count )
        return this.count
    }

    /**
     * Add in the end of the parser list
     * ```typescript
     * 
     * add
     *   >> child: SpanParseNode  // Receives a new Child
     *   << boolean               // Return true if child was added correctly
     * ```
     */
    add( node : TagNode | SpanNode | UnwrappedContent ): boolean {


        // If head child iss null, child become the first node
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        if( this.child == null ) {
            if( node instanceof TagNode ) {
                this.child = node
                this.count++
                this.upLevel()
                return true
            }
            return false
        }
        else  {
            let cur_node: any = this.last_child_ptr()
            while( cur_node.brother !== null )
                cur_node = cur_node.brother
        
            if( node instanceof TagNode ) {
                cur_node.brother = node
            }

            this.count++
            cur_node.child = child
            this.last_child_ptr = () => cur_node.child
        } 
        return true
        
    }
    /**
     * Remove the firts child added before
     * ```typescript
     * 
     * remove
     *   << boolean               // Return true if child was added correctly
     * ```
     */
    remove(): boolean {

        if(this.child !== null) {
            this.last_removed_child = this.child
            this.child = this.child.child
            this.count--
            return true
        }
        return false
    }

    addTag( node: TagNode ): boolean {
        // If head child iss null, child become the first node
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        if( !( node instanceof TagNode ) )
            return false
        
        if( this.child == null ) {
            this.child = node
            this.count++
            return true
        }
        let curNode: any = this.last_child_ptr()
        while( curNode.brother !== null )
                curNode = curNode.brother

            curNode.child = node
            this.last_child_ptr = () => curNode.child
        return true
    }

    removeTag() {
        if( this.child === null )
            return null

        let curNode = this.child
        while( curNode.child !== null ) {
            curNode = curNode.child
        }

        
        /*if( !( this.child instanceof TagNode ) )
            return false 

        if(this.child !== null) {
            this.last_removed_child = this.child
            if( this.last_removed_child.child ) {
                this.child = this.last_removed_child.child.brother
                this.count--
                return true
            }

        }*/

        return false
    }
}