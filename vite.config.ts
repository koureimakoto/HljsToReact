 /* eslint-env node, es6 */
 import path from 'path'
 import { defineConfig, splitVendorChunkPlugin } from 'vite'
 import react from '@vitejs/plugin-react'
 
 // https://vitejs.dev/config/
 export default defineConfig({
   plugins: [react(), splitVendorChunkPlugin()],
   build: {
    minify: false,
    outDir: "./dist/",
    lib: {
      entry: path.resolve(__dirname, 'src/lib/index.tsx'),
      name: 'hljstoreact',
      fileName: (format) => `hljstoreact.${format}.js`
    },
    rollupOptions: {
      external: ['react', 'react-dom'],
      output: {
        globals: {
          react: 'React'
        }
      }
    }
  },

   /*build: {
     
     outDir: "./src/dist/",   
     rollupOptions: {
       output: {
         entryFileNames: "[name].js",
         chunkFileNames: "[name].js",
         assetFileNames: "[name].[ext]",
         manualChunks: (id) => {
           if(id.includes('node_modules'))
             if(id.includes('highlight.js'))
               return "hljs/vendor_hljs"
             return "vendor"
           }
        }
      }
    }*/
 })
 