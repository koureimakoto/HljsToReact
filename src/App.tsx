import React from "react"
import HljsToReact from "./lib"
 
function App() {

  const input = {
    code: `#python
#encoding: utf-8

def test(z,x,y):
   """ This is meant to be the documentation of this function aaaaaaaa
   """
   # function def return z,x,y
   # def test is a test
   return z,x,y

try:
   """
      try:
         print("this is a test")
      except Exception as e:
         pass
   """
   now_in_a_var = """
      def this_is_a_string():
        return 'its not a function '
   """
   print(test("oi"))

except Exception as e:
   raise e
   
   print("batata")
   print('frita')
   print(f'batata frita')
   print(r'batata frita')
   
   `,
    lang: 'python'
  }
 
  return (
    <HljsToReact code={input.code} lang={input.lang} options={{e_num: true, hover: true}}/>
  )
}

export default App
