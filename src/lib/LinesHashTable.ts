import { ContentAST } from "./AST/ContentAST";
import { Line } from "./Line";

export class
LinesHashTable {
    table: Line[]
    next : Line[] | null
    size : number 

    constructor() {
        this.table = new Array<Line>(16)
        this.next  = null
        this.size  = 0
    }

    set( key: number, newLine: Line ) {
        this.table[key] = newLine
    }

    insert( key: number, ...newLines: Line[] ) {
        this.table.splice( key, 0, ...newLines)
    }


} 