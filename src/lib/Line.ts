import { ContentAST } from "./AST/ContentAST"

export class
Line {
    elNumber: number
    content : ( () => ContentAST ) | null

    constructor() {
        this.lid = window.crypto.randomUUID()
        this.elNumber = 0 
        this.content  = null
    }

    public  get lid(): string {
        return this.lid
    }
    private set lid( uid: string ) {
        this.lid = uid
    }
}