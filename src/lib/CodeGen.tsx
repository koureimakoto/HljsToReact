import { options, SpanParseNode, TokenList } from './DataTypes'
import './styles.pcss'

/**
 *  **Code Generator**
 * 
 *  Receives the codes and languages types to return formatted React standard 
 * contents using JSX(TSX). 
 * 
 */
export function
CodeGen(props: any) {
    const compiled    : JSX.Element[] = []
    let   wrapper     : JSX.Element[] = []
    let   enumFlag : boolean = props.options?.e_num ? true : false 
    let   count : number = 1
    let   buffer: SpanParseNode

    function
    defineOptions( options: options ) {
        let buffer: string = ''
        if( options.e_num ) buffer += ' n'
        if( options.hover ) buffer += ' h'
        if( options.copy  ) buffer += ' c'
        return buffer
    }

    function
    newSpan( className: any, tagContent: any ) {
        return (
            <span 
                key={ window.crypto.randomUUID() }
                className={className}>
                {tagContent}
            </span>
        )
    }

    function
    newLineNum( lineEnumFlag: boolean, lineCount: number ) {
        if( lineEnumFlag )
            return <div key={ window.crypto.randomUUID()} className='line-num'>{lineCount}</div>
        return <></>
    }

    function
    newLineTxt( wrapper: JSX.Element[], className: string ) {
        return (
            <div 
                key      ={window.crypto.randomUUID()} 
                className={className}
            >
                <p>    
                {wrapper}
                </p>
            </div>
        )
    }

    function
    newLine( JSXLineEnum: JSX.Element, JSXLineTxt: JSX.Element, style: any) {
        return (
            <div style={style} className={'line'} key={ window.crypto.randomUUID()}>
                    {JSXLineEnum}
                    {JSXLineTxt}
                </div>
        )

    }

    /**
     * Process only Span with class text contents or any content inside the tag.  
     */
    while(props.AST.remove()) {
        buffer = props.AST.getLastRemoved()

        if( buffer.childAST ) {
            wrapper.push( CodeGen( buffer.childAST ) )
        }

        // Only Span
        if( buffer.token === TokenList._el_span ) {
            wrapper.push( newSpan( buffer.txtContent, buffer.tagContent ) )
        }

        // Any other text contents
        else if( buffer.token === TokenList._el_content)
            wrapper.push(<span key={ window.crypto.randomUUID()}>{buffer.tagContent}</span>)
        
        // If detect a linebreak wrap it in a div
        if( buffer.token === TokenList._linebreak) {

            compiled.push( 
            newLine(
                newLineNum( enumFlag, count++ ),
                newLineTxt( wrapper, 'line-txt' ),
                { '--l': count }
                )
            )
            wrapper = [] as JSX.Element[]
        }
    }
    /**
     * 
     *  Add the last div wrapper        
     *
     *  If it checkLevel returned true, only return span and up Level in AST
     * */ 
    if( props.AST.checkLevel() )
        compiled.push(<>{ wrapper }</>)
    else
        compiled.push( 
            newLine(
                newLineNum( enumFlag, count++ ),
                newLineTxt( wrapper, 'line-txt' ),
                { '--l': count }
            )
        )

    return (
        <div className={'mw_hljstoreact ' + props.className}>
            <pre >
                <code 
                    className={'hljs hljsToReact' + defineOptions(props.options)} >
                    { compiled }
                </code>
            </pre>
        </div>
    )
}