import AbstractSyntaxTree from "./AbstractSyntaxTree"

/**
 * Data for request the React component from Span Parser
 * ```Typescript
 * TYPE
 *   code `string` //Array containing your code for the conversion
 *   lang `string` //Computer Language compatible with Highligh.JS
 * ```
 * ---
 * **Warning**
 * 
 * The application does not use the auto detect function provided by native lib. 
 */

export
type options = {
    e_num ?: boolean,
    hover ?: boolean,
    copy  ?: boolean
}


export
type dRequest = {
    code      : string,
    lang      : string
    className?: string
    options   : options
}

/**
 * A simple token list of the most important named token, processed inside the 
 * lexical analyzer ( HljsToReact, Lex, Lexer )
 */
export 
enum TokenList {
    _undef       =  0,
    _eof         = -1,
    _el          = -2,
    _el_span     = -3,
    _el_content  = -4,
    _attr_name_class  = -5,
    _attr_name_id     = -6,
    _attr_content     = -7,
    _linebreak   = -8,
    _linefeed    = -8,
    _exp         = -7,
}

/** Old Order
 * enum TokenList {
    _undef       =  0,
    _eof         = -1,
    _span        = -2,
    _class_name  = -3,
    _tag_content = -4,
    _content     = -5,
    _linebreak   = -6,
    _exp         = -7,
    _tag         = -8
}
 */


/**
 * Only use three flags to map the module. 
 */
export
enum LogFlagList {
    _ok            =  0,
    _Lexer_error = -1,
    _parser_error  = -2
}

/**
 * Node data struct using class to queue the span data or contents wrapping by 
 * <span> CONTENT </span> 
 * ---
 * ```typescript
 * CLASS
 *   token     : number // TokenList or ASCII number code with charCodeAt
 *   tagContent: string // Span tag content ...> string </... | <...
 *   txtContent: string // Class text content class=" string "
 *   child     : class  // SpanParseNode as a child 
 * ```
 */
export class
SpanParseNode {
    token       : number         
    tagContent ?: string 
    txtContent ?: string
    child       : SpanParseNode       | null = null
    childAST   ?: AbstractSyntaxTree | null

    constructor (
        token: number,
        tagContent ?: string,
        txtContent ?: string,
        AST        ?: AbstractSyntaxTree ) {
        this.token      = token
        this.tagContent = tagContent
        this.txtContent = txtContent
        this.childAST   = AST
    }


}

export class
UnwrappedContent {
    readonly token: number = TokenList._el_content
    brother: TagNode | null = null
    content: string  = ''
    level  : number
    constructor( level: number ) {
        this.level = level
    }
}

export class
ClassNode {
    readonly token: number = TokenList._attr_name_class
    insideClassContent: string = ''
    level  : number

    constructor(insideClassContent: string, level: number) {
        this.insideClassContent = insideClassContent
        this.level = level
    }
}

export class
SpanNode {
    readonly token: number = TokenList._el_span
    brother: SpanNode | TagNode | UnwrappedContent | null = null
    child  : SpanNode | TagNode | UnwrappedContent | null = null
    level  : number
    count  : number

    constructor( level: number ) {
        this.level = level
        this.count = 0
    }
}

export class
TagNode {
    readonly token: number = TokenList._el
    brother: TagNode  | UnwrappedContent | null = null
    child  : TagNode  | SpanNode | UnwrappedContent | null = null
    level  : number
    count  : number
    
    private lastRemovedBrother: TagNode  | UnwrappedContent | null = null
    private lastRemovedChild  : TagNode | SpanNode | UnwrappedContent | null = null
    private last_child_ptr = () => this.child

    constructor( level: number ) {
        this.level = level
        this.count = 0
    }

    addBrother( node: TagNode | UnwrappedContent ) {
        this.brother = node
    }

    removeBrother() {
        this.lastRemovedBrother = this.brother
        this.brother = null
        return this.lastRemovedBrother
    }

    addChild( node: SpanNode | UnwrappedContent | null ) {

        if( !node ) {
            return false
        }

        if( !this.child ) {
            this.child = node
            this.count++
            return true
        }
        else  {
            let curNode : any = this.last_child_ptr()

            while( curNode.child.brother )
                curNode = curNode.child.brother
        
            this.count++
            curNode.child.brother = node
            this.last_child_ptr = () => curNode.child.brother
        } 
        return true
    }

    removeChild() {
        if( this.child !== null ) {
            this.lastRemovedChild = this.child
            this.child = this.child.brother
            this.count--
            return this.lastRemovedChild
        }
        return null
    }
}