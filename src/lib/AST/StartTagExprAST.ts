import { AttrExprAST } from './AttrExprAST'

export class
StartTagExprAST {
    private  elementNameToken: number
    readonly attributeAST: AttrExprAST

    constructor( tokenElementName: number, attribute: AttrExprAST ) {
        this.elementNameToken = tokenElementName
        this.attributeAST     = attribute
    }

    token() {
        return this.elementNameToken
    }
}