import { CharDataExprAST } from './CharDataExprAST'
import { ElementExprAST  } from './ElementExprAST'

/**
 *  @class ContentAST
 * 
 * ---
 *  **Formal Expression :**
 * ```ts
 *      CharData --> ( EDS | BL )
 *      Content  --> ( EDS | BL )? ( ElementExpr | EDS | BL ) ( EDS | BL )? )*
 * -
 * ```
 * ___
 * @see: https://www.w3.org/TR/REC-xml/#NT-content
 */
export class
ContentAST {
    public  ContentBrother      : ContentAST     | null
    public  Content             : ElementExprAST | CharDataExprAST | null
    private lastRemovedBrother  : ContentAST     | null
    private lastBrotherPointer  !: () => ContentAST


    constructor( ExtContent: ElementExprAST | CharDataExprAST | null) {
        this.Content            = ExtContent
        this.ContentBrother     = null
        this.lastRemovedBrother = null
    }

    brother() {
        return this.ContentBrother
    }


    first( Node: ContentAST ): boolean {
        if( this.Content === null ) {
            this.Content = Node.Content
            return true
        }
        return false
    }

    /**
     * Add in the end of the Node AST list
     * ```ts
     * 
     * add
     *   >> child: ContentAST  // Receives a new Node
     *   << boolean            // Return true if child was added correctly
     * ```
     */
    add( Node: ContentAST ): boolean {

        if( this.ContentBrother === null ) {
            this.ContentBrother     = Node
            this.lastBrotherPointer = () => this.ContentBrother as ContentAST
            return true
        }
        else  {
            let CurNode: ContentAST = this.lastBrotherPointer()
            while( CurNode.ContentBrother !== null )
                CurNode = CurNode.ContentBrother

            CurNode!.ContentBrother = Node
            this.lastBrotherPointer = () => CurNode.ContentBrother as ContentAST
        } 
        return true 
    }
}