import { TokenList } from '../DataTypes'

/**
 *  **Regular Expressions :**
 * ```ts
 *      [ ElementContent   as EC  ] << /('>' [^"]* '<')/ 
 *      [ AttributeContent as AC  ] << /('"' [^"]* '"')/
 *      [ LineBreak        as LB  ] << /(#x9)+/
 * 
 *      CharData << ( EC | AC | LB )
 * -
 * ```
 */
export class
CharDataExprAST {
    
    private contentToken: TokenList._el_content | TokenList._attr_content | TokenList._linebreak
    private contentData : string

    constructor( token: number, data: string ) {
        this.contentToken = token
        this.contentData  = data
    }

    token() {
        return this.contentToken
    }

    data() {
        return this.contentData
    }

}