import { CharDataExprAST } from './CharDataExprAST'

export class
AttrExprAST {
    private  attributeNameToken: number 
    readonly AttributeContent  : CharDataExprAST

    constructor( tokenAttributeName: number, ExtClassContent: CharDataExprAST ) {
        this.attributeNameToken = tokenAttributeName
        this.AttributeContent   = ExtClassContent
    }

    token() {
        return this.attributeNameToken
    }
}