import { TokenList }       from '../../DataTypes'
import { ContentAST }      from '../ContentAST'
import { AttrExprAST }     from '../AttrExprAST'
import { ElementExprAST }  from '../ElementExprAST'
import { CharDataExprAST } from '../CharDataExprAST'
import { StartTagExprAST } from '../StartTagExprAST'

// Special Intern Import
import Terminal from '../../test/setup'

/**
 * Init Test
 */
describe( 'Content Node AST FIFO test', () => {
    const attrContent = 'I Attr Value'
    const elContent   = 'I El Value'
    const lineBreak   = '\n'

    const bufferNode = new ContentAST( 
        new ElementExprAST( 
            new StartTagExprAST(
                TokenList._el_span,
                new AttrExprAST(
                    TokenList._attr_name_class,
                    new CharDataExprAST(
                        TokenList._attr_name_class,
                        attrContent
                    )
                )
            ),
            new ContentAST(
                new CharDataExprAST(
                    TokenList._linebreak,
                    elContent
                )
            )
        )
    )

    it('Add the first Node', () => {

        const AST: ContentAST = new ContentAST( null )

        // Trying to Add in the self content
        expect( AST.first( bufferNode ) ).toBeTruthy()

        // Trying to Rewrite in the self content
        expect( AST.first( bufferNode ) ).toBeFalsy()

        // Checking the equality of data
        expect( (AST.Content as ElementExprAST).data()    ).toEqual( elContent )
        expect( (AST.Content as ElementExprAST).attrData() ).toEqual( attrContent )
        
    })  

    it('Add another node', () => {

        const AST: ContentAST = new ContentAST( null )

        // Already tested, just run
        AST.first( bufferNode )

        // Tryind to Add the Brother Content
        expect( AST.add( bufferNode ) ).toBeTruthy()
        
        // Checkig the equality of Brother's data
        expect( (AST.ContentBrother!.Content as ElementExprAST).data() ).toEqual( elContent )
        expect( (AST.ContentBrother!.Content as ElementExprAST).attrData() ).toEqual( attrContent )

        // New Simple Node
        const dataNode = new ContentAST(
            new CharDataExprAST(
                TokenList._linebreak,
                lineBreak
            )
        )

        // Tryind to add as the Last node
        expect( AST.add( dataNode ) ).toBeTruthy()
        expect( (AST.ContentBrother!.ContentBrother!.Content as CharDataExprAST).data() ).toEqual( lineBreak )

    }) 

    Terminal.log('Disabled Performance Test')
    /*var iterations = 100000;
    console.time('Array Test')
    for(var i = 0; i < iterations; i++ ){
        it( 'Using native JS Set', () => {
            const mySet = [] 

            // New Simple Node
            const dataNode = new ContentAST(
                new CharDataExprAST(
                    TokenList._linebreak,
                    lineBreak
                )
            )

            mySet.push( bufferNode )
            mySet.push( dataNode )
        })
    }
    console.timeEnd('Array Test')
    
    var iterations = 100000;
    console.time('Class Test')
    for(var i = 0; i < iterations; i++ ){
        it( 'Using native JS Set', () => {
            const mySet = new ContentAST( null ) 

            // New Simple Node
            const dataNode = new ContentAST(
                new CharDataExprAST(
                    TokenList._linebreak,
                    lineBreak
                )
            )

            mySet.first( bufferNode )
            mySet.add( dataNode )
        })
    }
    console.timeEnd('Class Test')
    */
})