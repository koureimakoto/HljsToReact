import { ContentAST      } from './ContentAST'
import { StartTagExprAST } from './StartTagExprAST'

/**
 *  @class ElementExprAST
 * 
 * ---
 *  **Formal Expression**
 * ```ts
 *  Element --> StartTag Content EndTag
 * ```
 * @see: https://www.w3.org/TR/REC-xml/#NT-content
 */
export class
ElementExprAST {
    STag: StartTagExprAST
    Content: ContentAST

    constructor( ExtOpeningSpan: StartTagExprAST, ExtContent: ContentAST ) {
        this.STag = ExtOpeningSpan
        this.Content = ExtContent
    }

    token(): number {
        return this.STag.token()
    }

    attrToken(): number {
        return this.STag.attributeAST.token()
    }

    attrData(): string {
        return this.STag.attributeAST.AttributeContent.data()
    }

    data(): string {
        return this.Content.Content!.data()
    }

}