/**
 * **Inside Console Log Class**
 * 
 * Verbose and stylized console log API.
 */

class 
InsideConsoleLog {
    bg          = 'background: '
    txt         = 'color: '
    b_radius    = 'border-radius: 2px;'
    init_color  = '#141414;'
    comp_color  = '#2eb054;'
    white       = '#f5f5f5;'
    center      = 'display:block; padding-top: 6px; width: 100%; height:25px ; text-align: center;'
    comp_align  = 'display:block; width: 20% ; text-align: center;'

    font_large  = 'font-size: 1rem;'
    font_mid    = 'font-size: .8rem;'
    font_small  = 'font-size: .5rem;'
    font_family = ''

    /** 
     *  The console.group Up and Down group level. If retunr fail this attribute
     * is used to reset the level of console
     */
    count_lvl   = 0
    
    initSimpleCompiler(): void {
        console.log(
            '%c Initializing Makoto Simple Compiler',
            this.center      +
            this.b_radius    +
            this.font_large  +
            this.font_family + 
            this.txt + this.white +
            this.bg  + this.init_color  
        )
        console.groupCollapsed(
            '%c Compiler log:  ', 
            this.font_family
        )
        this.levelUp()
    }

    endSimpleCompiler(): void {
        this.exitAllGroups()
        console.groupEnd()
        console.log(
            '%c Ending Makoto Simple Compiler',
            this.center      +
            this.b_radius    +
            this.font_large  +
            this.font_family + 
            this.txt + this.white +
            this.bg  + this.init_color  
            )
        this.levelDown()
    }

    initLexer(): void {
        this.levelUp()
        console.groupCollapsed(
            '%c Initializing Lexer',
            this.font_mid  +
            this.font_family + 
            this.txt + this.white
        )
    }

    endLexer(): void {
        console.groupEnd()
        console.log(
            '%c Ending Lexer',
            this.font_mid  +
            this.font_family + 
            this.txt + this.white
        )
        this.levelDown()
    }
    
    initParser( name: string ): void {
        this.levelUp()
        console.groupCollapsed(
            '%c Initializing Parser ' + name,
            this.b_radius    +
            this.comp_align  +
            this.font_mid    +
            this.font_family + 
            this.txt + this.white +
            this.bg  + this.comp_color  
        )
    }

    endParser({ name }: { name: string }): void {
        //console.groupEnd()
        console.log(
            '%c Ending Parser ' + name,
            this.b_radius    +
            this.comp_align  +
            this.font_mid    +
            this.font_family + 
            this.txt + this.white +
            this.bg  + this.comp_color  
        )
        this.levelDown()
    }

    // Incomplete
    errorLexer( letter: string, attachMsg: string = '' ): void {
        this.exitAllGroups()
        console.error(
            'Lexer Error: \n ' + 
            'token( ' + letter + ' )\n' + 
            attachMsg
        )
    }

    // Incomplete
    errorParser( attachMsg: string = '' ): void {
        this.exitAllGroups()
        console.error(
            'Parser Error: \n ' + 
            attachMsg
        )
    }

    exitAllGroups() {
        while( this.count_lvl >= 0) {
            this.levelDown()
            console.groupEnd()
        }
        console.groupEnd()

    }

    levelUp(): void {
        this.count_lvl++
    }

    levelDown(): void {
        this.count_lvl--
    }
}

const Log = new InsideConsoleLog;
export default Log