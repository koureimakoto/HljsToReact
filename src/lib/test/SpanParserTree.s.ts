import { SpanParseNode   as Node, TokenList } from "../DataTypes";
import AbstractSyntaxTree from "../AbstractSyntaxTree";

describe( 'Span Parser Queue test', () => {
    const level: number = 0
    const children = {
        child_1: new Node ( TokenList._span, '', 'Makoto World'),
        child_2: new Node ( TokenList._span, '', 'Makoto World 2'),
        child_3: new Node ( TokenList._span, '', ''),
        child_4: new Node ( TokenList._tag_content, 'Makoto World', 'hi')
    }

    it( 'Adding new fail data as a node in queue', () => {
        const queue = new AbstractSyntaxTree(level)
        const node_undef       = new Node( TokenList._undef               )
        const node_span_empty  = new Node( TokenList._span       , '' , '')
        const node_span_filled = new Node( TokenList._span       , 'm', '')
        const node_tag_empty   = new Node( TokenList._tag_content, '' , '')

        expect( queue.add( node_undef       )).toBe( false )
        expect( queue.add( node_span_empty  )).toBe( false )
        expect( queue.add( node_span_filled )).toBe( false )
        expect( queue.add( node_tag_empty   )).toBe( false )
    })

    it( 'Adding new correct data as a node in queue', () => {
        const queue = new AbstractSyntaxTree(level)
        const node_span_txt   = new Node( TokenList._span       , '', 'm' )
        const node_tag_empty  = new Node( TokenList._tag_content, 'm', ''  )
        const node_linebreak  = new Node( TokenList._linebreak  , '', ''  )

        expect( queue.add( node_span_txt   )).toBe( true )
        expect( queue.add( node_tag_empty  )).toBe( true )
        expect( queue.add( node_linebreak  )).toBe( true )
    })

    it( 'remove() : removing all child', () => {
        const queue = new AbstractSyntaxTree(level)

        queue.add( children.child_1 )
        queue.add( children.child_2 )

        queue.remove()
        let buffer_node = queue.getLastRemoved()
        if( buffer_node !== null ) {
            expect( buffer_node.token ).toBe( children.child_1.token )
            expect( buffer_node.tagContent ).toBe( children.child_1.tagContent )
            expect( buffer_node.txtContent ).toBe( children.child_1.txtContent )
        }

        queue.remove()
        buffer_node = queue.getLastRemoved()
        if( buffer_node !== null ) {
            expect( buffer_node.token ).toBe( children.child_2.token )
            expect( buffer_node.tagContent ).toBe( children.child_2.tagContent )
            expect( buffer_node.txtContent ).toBe( children.child_2.txtContent )
        }

    })

    it( 'Number of nodes(children) in the queue', () => {
        const queue = new AbstractSyntaxTree(level)


        queue.add( children.child_1 )
        expect( queue.getNodeCount() ).toBe(1)

        // Try adding incorrect data, return false 
        queue.add( children.child_3 ) 
        expect( queue.getNodeCount() ).not.toBe(2)

        queue.add( children.child_4 ) 
        expect( queue.getNodeCount() ).toBe(2)

        queue.remove()
        expect( queue.getNodeCount() ).toBe(1)
        queue.remove()
        expect( queue.getNodeCount() ).toBe(0)
    })

})