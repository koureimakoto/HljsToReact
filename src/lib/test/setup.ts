/* eslint-disable */
import '@testing-library/jest-dom'

export default 
class Terminal {

    static ESC   = '\x1b['
    static RESET = "\x1b[0m"
    static under = "\x1b[4m"

    static cCodeBase = '38;5;'

    static cRed    = this.color( '160' )
    static cBlue   = this.color( '63'  )
    static cGreen  = this.color( '65'  )
    static cYellow = this.color( '220' )

    static clog = console.log
    static cwarn= console.warn



    static log( msg: string ) {
            this.clog( '%s', msg ) 
    }

    static color( code: string ): string {
        return ( this.ESC + this.cCodeBase + code + 'm' )
    }

    static red( msg: string ): string {
        return ( this.cRed + msg + this.RESET )
    }

    static blue( msg: string ): string {
        return( this.cBlue + msg + this.RESET )
    }

    static green( msg: string ): string {
        return( this.cGreen + msg + this.RESET )
    }

    static yellow( msg: string ): string {
        return( this.cYellow + msg + this.RESET )
    }

    static bold( msg: string ): string {
        return( this.ESC + '2m' + msg + this.RESET )
    }

}





global.console = <any>{
    log: jest.fn(),
    warn: console.warn,
    debug: jest.fn(),
    error: jest.fn(),
    trace: jest.fn(),
    group: jest.fn(),
    groupCollapsed: jest.fn(),
    groupEnd: jest.fn(),
    time: console.time,
    timeEnd: console.timeEnd,
    terminal: console.log
}


