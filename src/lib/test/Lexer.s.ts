import { TokenList } from "../DataTypes"
import { Lexer     }  from "../Lexer"

describe( 'Simples Span Tag Lexer', () => {
    const lexer = new Lexer
    
    const whitespace = ' '
    const ucletter   = 'M'
    const lcletter   = 'w'
    const number     = '1'
    const empty      = ''
    const name       = 'MW'
    const simbol     = '>'

    it( 'token() : return correct token', () => {
        lexer.tokenizeThis('< span class = " / >')
        expect( lexer.token()).toBe('<'.charCodeAt(0)     )
        expect( lexer.token()).toBe(TokenList._span       )
        expect( lexer.token()).toBe(TokenList._class_name )
        expect( lexer.token()).toBe('='.charCodeAt(0)     )
        expect( lexer.token()).toBe('"'.charCodeAt(0)     )
        expect( lexer.token()).toBe('/'.charCodeAt(0)     )
        expect( lexer.token()).toBe('>'.charCodeAt(0)     )
    })

    it( 'lastChar() ', () => {
        const txt = '="'
        const tmp_lexer = new Lexer()
        tmp_lexer.tokenizeThis(txt)
        tmp_lexer.token()
        expect( tmp_lexer.lastChar()).toBe('=')
        expect( tmp_lexer.lastChar()).not.toBe('"')
    })

    it( 'classContent : return all content', () => {
        let   txt       = 'content text"'
        const tmp_lexer = new Lexer()
        tmp_lexer.tokenizeThis(txt)
        expect( tmp_lexer.classContent() ).toBe('content text')
    })

    it( 'nextChar : return only the next found char', () => {
        const txt = 'MW'
        const tmp_lexer = new Lexer()
        tmp_lexer.tokenizeThis(txt)
        expect( tmp_lexer.nextChar() ).toBe('M')
        expect( tmp_lexer.nextChar() ).not.toBe('M')
    })

    it( 'space() : detect only whitespace', () => {
        expect( lexer.space( whitespace)).toBe(true)
        expect( lexer.space( ucletter  )).toBe(false)
        expect( lexer.space( empty     )).toBe(false)
    })

    it( 'alpha() : detect only alpha',  () => {
        expect( lexer.alpha( ucletter  )).toBe(true)
        expect( lexer.alpha( lcletter  )).toBe(true)
        expect( lexer.alpha( name      )).toBe(false)
        expect( lexer.alpha( empty     )).toBe(false)
        expect( lexer.alpha( simbol    )).toBe(false)
        expect( lexer.alpha( number    )).toBe(false)
        expect( lexer.alpha( whitespace)).toBe(false)
    })

    it( 'checkLetter() : compare all character type', () => {
        expect( lexer.checkLetter( name      , name         )).toBe(true)
        expect( lexer.checkLetter( empty     , empty        )).toBe(true)
        expect( lexer.checkLetter( whitespace, whitespace   )).toBe(true)
        expect( lexer.checkLetter( whitespace, empty        )).toBe(false)
    })

    it( 'openTagSimbol() : detect only \'<\'', () => {
        expect( lexer.openTagSimbol('<')).toBe(true)
        expect( lexer.openTagSimbol('>')).toBe(false)
    })

    it( 'closeTagSimbol() : detect only \'>\'', () => {
        expect( lexer.closeTagSimbol('>')).toBe(true)
        expect( lexer.closeTagSimbol('<')).toBe(false)
    })

    it( 'checkEOF : If the code text return last position from array', () => {
        const txt = 'MW'
        const tmp_lexer = new Lexer()
        tmp_lexer.tokenizeThis(txt)
  
        expect(tmp_lexer.checkEof()).toBe(false)

        tmp_lexer.token()
        expect(tmp_lexer.checkEof()).toBe(true)
    })
})