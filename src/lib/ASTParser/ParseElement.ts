import { Lexer }               from '../Lexer'
import { ParseEndTag }         from './ParseEndTag'
import { ParseStartTag }       from './ParseStartTag'
import { ElementExprAST }      from '../AST/ElementExprAST'
import { CommonsParseElement } from './CommonsParseElement'
import { ParseElementContent } from './ParseElementContent'
import { TokenList } from '../DataTypes'
import Terminal from '../test/setup'
import { PrintNode } from './test/PrintNode'

export class
ParseElement extends CommonsParseElement {
    STagParser    : ParseStartTag
    ContentParser!: ParseElementContent
    ETagParser    : ParseEndTag
    AST           : ElementExprAST | null 
    
    constructor( ExtLex: Lexer ) {
        super( ExtLex, TokenList._el_span )
        this.STagParser    = new ParseStartTag( ExtLex, this.elementNameToken ) 
        this.ETagParser    = new ParseEndTag( ExtLex, this.elementNameToken )
        this.AST           = null
    }


    exec(): boolean {
        this.register([
            () => this.execSTagParser(),
            () => this.execElementContentParser(),
            () => this.execETagParser()
        ])

        if( !super.exec() )
            return false
        
        this.AST = new ElementExprAST(
            this.STagParser.nodeAST(),
            this.ContentParser.nodeAST()
        )

        return true
    }
    
    nodeAST(): ElementExprAST | null {
        return this.AST
    }


    execSTagParser() {
        if( !this.STagParser.exec() ){
            throw 'No Start Tag was identified'
        }
    }
    
    execElementContentParser() {
        this.ContentParser = new ParseElementContent( this.Lex )
        if( !this.ContentParser.exec() ) 
            throw 'No Element Content was identified'
    }
    
    execETagParser() {
        if( !this.ETagParser.exec() )
            throw 'No End Tag was identified'

    }

}

