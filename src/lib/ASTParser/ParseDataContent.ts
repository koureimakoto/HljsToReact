import { Lexer }             from '../Lexer'
import { TokenList }         from '../DataTypes'
import { ContentAST }        from '../AST/ContentAST'
import { CharDataExprAST }   from '../AST/CharDataExprAST'
import { CommonsParseTools } from './CommonsParseTools'

/**
 * Content Parser, are any text between the greather sign and less sign'
 * 
 * @see https://www.w3.org/TR/REC-xml/#NT-content
 */
export class
ParseDataContent extends CommonsParseTools {
    private readonly lessSign : string = '<'
    private readonly lineBreak: string = '\n'
    private buffer            : string = ''
    private content           : string = ''
    private lineBreakFlag     : boolean= false
    private ContentNodeAST        : ContentAST
    
    // Alias
    private readonly lineFeed : string = this.lineBreak
    checkLineFeed = this.checkLineBreak.bind(this)

    constructor( ExtLex: Lexer ) {
        super(  ExtLex )

        this.ContentNodeAST = new ContentAST(
            null
        )
    }

    exec(): boolean {

        this.register([
            () => this.checkContent()
        ])

        return super.exec()
    
    }

    /**
     * Search through the content until Less-than Sign or MAX_LEN
     * 
     * ```ts
     * checkContent
     *  void | throw Error
     * ```
     */
    checkContent(): void {
        this.buffer   = this.Lex.nextChar()
        
        while( ! this.Lex.endOf( this. buffer, this.lessSign ) ) {
            if( this.checkLineBreak( this.buffer )) {
                this.lineBreakFlag = true
                break 
            }                

            this.nextChar()
            this.maxSafeLoop()
        }
        this.addContent()
    }

    /**
     *  Only use to clear buffer and content
     */
    clear() {
        this.buffer = ''
        this.content = ''
    }

    /**
     * Wrapper, prepare the next character and concatenate the content
     */
    nextChar() {
        this.content += this.buffer
        this.buffer   = this.Lex.nextChar()
    }

    /**
     * Return the private Content Node AST
     * 
     * ```ts
     * nodeAST
     *  << ContentAST
     * ```
     */
    nodeAST(): ContentAST {
        return this.ContentNodeAST
    }


    addContent() {
        if( !this.checkEmptyContent() ) {
            this.ContentNodeAST = (
                new ContentAST(
                    new CharDataExprAST(
                        TokenList._el_content,
                        this.filter( this.content )
                    ),
                )
            )
            
            this.Lex.returnToPrev()
        }
        else 
        if( this.lineBreakFlag === true ) {
            this.ContentNodeAST = (
                new ContentAST(
                    new CharDataExprAST(
                        TokenList._linebreak,
                        '\n'
                    ),
                )
            )
            this.lineBreakFlag = false
        }
        else {
            this.ContentNodeAST = new ContentAST(
                null
            )
            this.Lex.returnToPrev()
            this.clear()
            throw 'Empty or End of Content'
        }

        this.clear()

    }

    /**
     * Search for '\n'
     * Code U+000A 
     * 
     * ```ts
     * checkLineBreak
     *  << boolean
     * ```
     */
    checkLineBreak( letter: string ): boolean {
        if( letter === this.lineBreak )
            return true
        return false
    }

    /**
     * Verify if the content is empty 
     * 
     * ```ts
     * checkEmptyContent
     *  << boolean
     * ```
     */
    checkEmptyContent(): boolean {
        if( this.content === '' )
            return true
        return false
    }

}