import { Lexer }             from '../Lexer'
import { CommonsParseTools } from './CommonsParseTools'

/**
 * End Tag Parser, only to End Tag Expr AST Elements
 * 
 * @see https://www.w3.org/TR/REC-xml/#NT-element
 */
export class
CommonsParseElement extends CommonsParseTools {
    protected elementNameToken: number

    constructor( ExtLex: Lexer, elementNameToken: number ) {
        super( ExtLex )
        this.elementNameToken = elementNameToken
    }

    /**
     * Search for '<'
     * Code U+003C
     * 
     * ```ts
     * checkStart
     *  void | throw Error
     * ```
     */
    checkStart(): void {
        if( this.newToken() !== '<'.charCodeAt(0) )
            throw new Error('ParseEndTag.checkStart( token: < )')
    }

    /**
     * Search for Tag Name
     * @see https://www.w3.org/TR/REC-xml/#NT-Name 
     * 
     * ```ts
     * checkName
     *  void | throw Error
     * ```
     */
    checkName(): void {
        if( this.newToken() !== this.elementNameToken )
            throw 'ParseEndTag.checkStart( Tag Name )'
    }
    
    /**
     * Search for '>'
     * Code U+003E 
     * 
     * ```ts
     * checkEnd
     *  void | throw Error
     * ```
     */
    checkEnd( ): void {
        if( this.newToken() !== '>'.charCodeAt(0) )
            throw 'ParseEndTag.checkStart( token: > )'
    }
}