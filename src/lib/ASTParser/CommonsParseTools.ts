import   Log     from '../Log'
import { Lexer } from '../Lexer'

export class
CommonsParseTools {
    protected readonly MAX_LEN   : number = 255
    protected Lex: Lexer 
    protected safeLoopCount: number = 0
    private fn: (() => void)[] = []
    
    /**
     *  Prevent infinite loop
     */
    maxSafeLoop(): void  {
        this.safeLoopCount++
        if( this.MAX_LEN <= this.safeLoopCount )
            throw 'Attribute Value Exceeded the Safety String Size'
    }

    /**
     * Prevent empty content
     */
    minSafeLoop(): void {
        if( this.safeLoopCount <= 0 )
            throw 'Attribute Value Was Empty'
        this.safeLoopCount = 0
    }

    /**
     * ```ts
     * constructor
     *  >> tagNameToken: number // Type of Name Lexer will fetch
     *  >> ExtLex   : Lexer  // Extern Lexer
     * ```
     */
     constructor( ExtLex: Lexer ) {
        this.Lex       = ExtLex
    }

    /**
     * Exec the correct sequence of Parser returned by register method
     * ```ts
     * exec
     *  << true | false && throw Error
     * ```
     */
    exec(): boolean {

        Log.initParser('Closing Span Tag')
        try { 
            this.fn.forEach( (exec: () => void) => {
                Log.initLexer()
                exec()
                Log.endLexer()
            });
        } catch( e ) {    
            Log.errorParser( e as string )   
            return false
        } finally {
            Log.endParser({ name: 'Closing Tag' })
        }
        
        return true
    }

    /**
     * 
     * Only Simples Rules 
     * 
     * ```ts
     * 
     *  // To register:
     * 
     *  super.register([
     *      () => ruleMethod() , // #1
     *      () => ruleMethod() , // #2
     *      ...
     *  ])
     * 
     * ```
     */
    register( arr: (() => void)[] ) {
        if(arr.length <= 0)
            throw new Error('Parser Rules was empty.')
        this.fn = arr
    }

    /**
     * Method of the Class Lexer, wrapped.
     * 
     * ```ts
     * newToke
     *  << tagNameToken: number
     * ```
     */
    newToken(): number {
        return this.Lex.token()
    }
    
    /**
     * Receive a string HTML entities and convert to plain/text
     * ```typescript
     * filter
     *   >> txt         : string 
     *   << filterd_txt : string
     * ```
     */
    filter( txt: string ) : string {
        return   txt.replace(/&lt;/g   , '<' )
                    .replace(/&gt;/g   , '>' )
                    .replace(/&amp;/g  , '&' )
                    .replace(/&#39;/g  , '\'') 
                    .replace(/&#x27;/g , '\'') 
                    .replace(/&quot;/g , '"' )  
    }
}