import { TokenList } from '../DataTypes'
import { Lexer } from '../Lexer'
import { CommonsParseElement } from './CommonsParseElement'

/**
 * End Tag Parser, only to End Tag Expr AST Elements
 * 
 * @see https://www.w3.org/TR/REC-xml/#NT-ETag
 */
export class
ParseEndTag extends CommonsParseElement {


    exec(): any {
        this.register([
            () => this.checkStart(), // <
            () => this.checkSlash(), // /
            () => this.checkName(),  // name
            () => this.checkEnd(),   // >
        ])



        return super.exec()
    }

    /**
     * Search for '/'
     * Code U+002F 
     * 
     * ```ts
     * checkSlash
     *  void | throw Error
     * ```
     */
    checkSlash(  ): void {
        if( super.newToken() !== '/'.charCodeAt(0) )
           throw 'ParseEndTag.checkStart( token: / )'
    }
  
}