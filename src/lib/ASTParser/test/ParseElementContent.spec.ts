import { TokenList } from '../../DataTypes'
import Lex from '../../Lexer'
import Terminal from '../../test/setup'
import { ParseElement } from '../ParseElement'
import { ParseElementContent } from '../ParseElementContent'
import { PrintNode } from './PrintNode'

describe( 'Important Parse Test: Element Content Rules', () => {
    const elContentFirst  = 'Makoto World'
    const elContentSecond = 'Kourei Makoto\n'
    const elTrully        = '<span class="Attr">'+ elContentFirst +'</span>'


     const Parse = new ParseElementContent(
         Lex
     )

    it( 'Rule: ( Empty )', () => {
        Lex.tokenizeThis('</span>')
        expect(Parse.elementContentRules()).toBe(false)
    } )
    
    it( 'Rule: Content ( One Data Element )', () => {
        Lex.tokenizeThis('\n</span>')
        expect(Parse.elementContentRules()).toBe(true)
    } )

    it( 'Rule: Content* ( Two Data Element )', () => {
        Lex.tokenizeThis('\n' + elContentFirst + '<')
        expect(Parse.elementContentRules()).toBe(true)
        //PrintNode.listDataContentAST( Parse.nodeAST() )
    } )
    
    it( 'Rule: Content* Element ( One Element )', () => {
        Lex.tokenizeThis( elTrully + '\n<')
        expect(Parse.elementContentRules()).toBe(true)
        //PrintNode.listDataContentAST( Parse.nodeAST() )
    } )

    it( 'Rule: Element Element Content ( Sinbling Element  )', () => {
        Lex.tokenizeThis( elTrully + elTrully + '\n<')
        expect(Parse.elementContentRules()).toBe(true)
        //PrintNode.listDataContentAST( Parse.nodeAST() )
    } )

    it( 'Rule: Content Element Element Content ( Sinbling Element  )', () => {
        Lex.tokenizeThis( '\n' + elTrully + elTrully + '\n<')
        expect(Parse.elementContentRules()).toBe(true)
        //PrintNode.listDataContentAST( Parse.nodeAST() )
    } )

    it( 'Rule: Content Element Content Element Content ( Sinbling Element  )', () => {
        Lex.tokenizeThis( '\n' + elTrully + '\n' +  elTrully + '\n<')
        expect(Parse.elementContentRules()).toBe(true)
        //PrintNode.listDataContentAST( Parse.nodeAST() )
    } )

    it( 'Rule: Wrapper', () => {
        Lex.tokenizeThis( 
            '   ' + elTrully + 
            '<span class="Wrapper">'+ elTrully + elTrully +'</span>'+ "   " +
            elTrully + '\n<')
        expect(Parse.elementContentRules()).toBe(true)
        PrintNode.listDataContentAST( Parse.nodeAST() )
    } )

} )