import { TokenList }       from '../../DataTypes'
import { StartTagExprAST } from '../../AST/StartTagExprAST'
import { ContentAST }      from '../../AST/ContentAST'
import { CharDataExprAST } from '../../AST/CharDataExprAST'
import Terminal from '../../test/setup'
import { ElementExprAST } from '../../AST/ElementExprAST'

export class
PrintNode {
    

    /**
     * Transform token code to string
     * ```ts
     * token
     *  >> token code: number
     *  << token name: string
     * ````
     */
    static token( token: number ) {
        if( token === TokenList._el_span )
            return 'Span Element'
        if( token === TokenList._attr_name_class )
            return 'Class Attribute'
        if( token === TokenList._el_content )
            return 'Element Content'
        if( token === TokenList._attr_content )
            return 'Attribute Content'
        if( token === TokenList._linebreak )
            return 'Line break / Line feed'

        return 'UNDEFINED'
    }

    /**
     * Print Start Tag [Element] 
     * 
     * ```ts
     * startTagNodeAST
     *  >> Node: StartTagExprAST
     * ```
     */
    static startTagNodeAST( Node: StartTagExprAST ) {
        const elementName = this.token( Node.token() )
        const attrName    = this.token( Node.attributeAST.token() )
        const attrContent = this.token( Node.attributeAST.AttributeContent.token() )
        const attrData    = Node.attributeAST.AttributeContent.data()
        
        return (
            Terminal.blue('Token Type: '            ) + Terminal.green( elementName ) + '\n' +
            Terminal.blue('Attribute Name: '        ) + Terminal.green( attrName    ) + '\n' +
            Terminal.blue(' |  Attribute Type: '    ) + Terminal.green( attrContent ) + '\n' +
            Terminal.blue(' |  Attribute Data: '    ) + Terminal.green( attrData    ) + '\n'
        )
    }

    /**
     * Create the ContentAST Node STRING
     * 
     * ```ts
     * elementDataContentAST
     *  >> Node: ContentAST 
     *  >> len : number       // Tab len
     *  << Foormated Text: string
     * ```
     */
    private static elementDataContentAST( Node: ContentAST, len: number): string {
        if( !( Node.Content instanceof CharDataExprAST ) )
            console.error( 'This method is not compatible with this node' )

        const contentName    = this.token( Node.Content!.token() )
        const contentData               = Node.Content!.data()
        const contentBrother = Node.brother()
        
        let elementAttr    =''

        if( Node.Content instanceof ElementExprAST ) 
            elementAttr = Node.Content.attrData() 
        
        let tab   = ''
        let count = 0

        while( count++ < len )
            tab += '   |'

        return (
            Terminal.blue( tab + 'Token   ' ) + ': ' + 
            Terminal.green( contentName )
            + '\n' +
            Terminal.blue( tab + 'Attr    ') + ': ' +
            Terminal.green( elementAttr )
            + '\n' +
            Terminal.blue( tab + 'Content ' ) + ': ' + 
            Terminal.green( ( contentData === '\n' ? '"\\n"' : '"' + contentData + '"' ) )
            + '\n' +
            Terminal.blue( tab +  Terminal.bold( '&Brother' ) + ': ' ) + 
            ( !contentBrother ? Terminal.red('null') : Terminal.yellow('next \n' ) )
        )
    }

    private static elementExprContent( Node: ElementExprAST ) {
        let toPrint = this.startTagNodeAST( Node.STag )

        if( Node.Content instanceof ElementExprAST ) {
            return this.elementExprContent( Node.Content )
        }


    }

    /**
     *  Print Content Data List
     * 
     * ```ts
     * listDataContentAST
     *  >> Node: ContentAST
     * ```
     */
    static listDataContentAST( 
        Node: ContentAST, 
        initCounter: number = 0,
        child: boolean = false  
    ): string | void {
        let toPrint = ''
        let bufferNode: ContentAST | null = Node
        
        let count = initCounter
        while( bufferNode != null ) {
            toPrint += this.elementDataContentAST( bufferNode!, count++)
            
            /*if( bufferNode.Content instanceof ElementExprAST )
                toPrint += this.listDataContentAST(

                )*/
            bufferNode = bufferNode.brother()
        }
        if( child === true )
            return toPrint

        Terminal.log( toPrint )
    }

}