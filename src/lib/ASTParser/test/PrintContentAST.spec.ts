import   Lex                from '../../Lexer'
import { TokenList }        from '../../DataTypes'
import { PrintNode }        from './PrintNode'
import { ParseDataContent } from '../ParseDataContent'

/**
 * Modified the global behavior of the Console.API
 */
 global.console = <any>{
    // Overrided
    warn: console.log,
    // Disabled
    groupCollapsed: jest.fn(),
    groupEnd: jest.fn(),
    debug   : jest.fn(),
    trace   : jest.fn(),
    group   : jest.fn(),
    error   : jest.fn(),
    log     : jest.fn()
}

/**
 * Init Test
 */
describe( 'To print the correct format of Start Tag Node AST', () => {
    const Parser = new ParseDataContent(
        Lex
    )

    it( 'Check Node Elements Data', () => {
        const dataFirst = 'Content data 01'
        const dataBL    = '\n'
        const dataLast  = 'Content data 02'

        Lex.tokenizeThis( dataFirst + dataBL + dataLast + '<' )

        Parser.exec()
        let bufferNode = Parser.nodeAST()

        expect( bufferNode.brother() ).toBeNull()
        expect( bufferNode.Content!.token()).toBe( TokenList._el_content )
        expect( bufferNode.Content!.data() ).toBe( dataFirst )

        
        Parser.exec()
        bufferNode.add( Parser.nodeAST() )
        
        expect( bufferNode.ContentBrother!.Content!.token()).toBe( TokenList._linebreak )
        expect( bufferNode.ContentBrother!.Content!.data() ).toBe( dataBL )
        
        
        Parser.exec()
        bufferNode.add( Parser.nodeAST() )
        
        expect( bufferNode.ContentBrother!.ContentBrother!.brother() ).toBeNull()
        expect( bufferNode.ContentBrother!.ContentBrother!.Content!.token()).toBe( TokenList._el_content )
        expect( bufferNode.ContentBrother!.ContentBrother!.Content!.data() ).toBe( dataLast )
        
        //PrintNode.listDataContentAST( bufferNode )

    })


})