import { TokenList } from "../../DataTypes"
import Lex from "../../Lexer"
import { ParseEndTag } from "../ParseEndTag"
import { ParseStartTag } from "../ParseStartTag"

describe( 'Span Element Parser', () => {
    const STagParser = new ParseStartTag( Lex, TokenList._el_span, TokenList._attr_name_class )
    const ETagParser = new ParseEndTag( Lex, TokenList._el_span )

    it( 'Save and Roll Back State', () => {
        const attContent = '...'
        const elContent  = ''
        Lex.tokenizeThis('< span class="'+ attContent +'">'+ elContent +'</span>')

        expect( STagParser.exec() ).toBe(true)
        Lex.saveState()
        expect( STagParser.exec() ).toBe(false)
        expect( ETagParser.exec() ).toBe(false)
        Lex.rollBackState()
        expect( ETagParser.exec() ).toBe(true)
    })
})