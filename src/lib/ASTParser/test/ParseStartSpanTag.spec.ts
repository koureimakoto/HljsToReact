import { TokenList } from '../../DataTypes'
import   Lex                  from '../../Lexer'
import { ParseStartTag } from '../ParseStartTag'

/**
 * Modified the global behavior of the Console.API
 */
 global.console = <any>{
    // Overrided
    error: console.log,
    // Disabled
    groupCollapsed: jest.fn(),
    groupEnd: jest.fn(),
    debug   : jest.fn(),
    trace   : jest.fn(),
    group   : jest.fn(),
    log     : jest.fn()
}

/**
 *  Init Test
 */
describe('Start Tag - Parser', () => {
    const Parser = new ParseStartTag( 
        Lex,
        TokenList._el_span
    )

    it( 'Found Start Span Tag', () => {
        Lex.tokenizeThis('<span class=".">')
        expect(Parser.exec()).toBe(true)

        Lex.tokenizeThis('<  span    class  =  " . "  >')
        expect(Parser.exec()).toBe(true)
    })

    it( 'Receive the correct Attribute Value', () => {
        Lex.tokenizeThis('<span class=".">')
        expect(Parser.exec()).toBe(true)
        expect(Parser.attributeValue()).toBe('.')

        Lex.tokenizeThis('<span class="">')
        expect(Parser.exec()).toBe(false)
    })

    it( 'Unfound Start Tag', () => {
        Lex.tokenizeThis('@')
        expect(Parser.exec()).toBe(false)

        Lex.tokenizeThis('<div')
        expect(Parser.exec()).toBe(false)

        Lex.tokenizeThis('<span className')
        expect(Parser.exec()).toBe(false)

        Lex.tokenizeThis('<span class$')
        expect(Parser.exec()).toBe(false)

        Lex.tokenizeThis('<span class=*')
        expect(Parser.exec()).toBe(false)

        Lex.tokenizeThis('<span class="')
        expect(Parser.exec()).toBe(false)

        Lex.tokenizeThis('<span class="..."')
        expect(Parser.exec()).toBe(false)
        
    })
})