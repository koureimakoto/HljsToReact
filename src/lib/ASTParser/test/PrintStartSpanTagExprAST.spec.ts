import   Lex                  from '../../Lexer'
import { TokenList }          from '../../DataTypes'
import { PrintNode }          from './PrintNode'
import { StartTagExprAST }    from '../../AST/StartTagExprAST'
import { ParseStartTag } from '../ParseStartTag'

/**
 * Modified the global behavior of the Console.API
 */
 global.console = <any>{
    // Overrided
    warn: console.log,
    // Disabled
    groupCollapsed: jest.fn(),
    groupEnd: jest.fn(),
    debug   : jest.fn(),
    trace   : jest.fn(),
    group   : jest.fn(),
    error   : jest.fn(),
    log     : jest.fn()
}

/**
 * Init Test
 */
describe( 'To print the correct format of Start Tag Node AST', () => {
    const Parser = new ParseStartTag(
        Lex,
        TokenList._el_span,
    )

    it( 'Check Node Elements Data', () => {
        const data = 'Makoto World'
        Lex.tokenizeThis('<span class="' + data + '">')
        Parser.exec()

        const bufferNode: StartTagExprAST = Parser.nodeAST()

        expect(
            bufferNode.token()
        ).toBe(TokenList._el_span)
        expect(
            bufferNode.attributeAST.token()
        ).toBe(TokenList._attr_name_class)
        expect(
            bufferNode.attributeAST.AttributeContent.token()
        ).toBe(TokenList._attr_content)
        expect(
            bufferNode.attributeAST.AttributeContent.data()
        ).toEqual(data)
        
        //PrintNode.startTagNodeAST( bufferNode )
    }) 


})