import   Lex                from '../../Lexer'
import { TokenList }        from '../../DataTypes'
import { ParseEndTag } from '../ParseEndTag'

/**
 * Modified the global behavior of the Console.API
 */
global.console = <any>{
    // Overrided
    error: console.log,
    // Disabled
    groupCollapsed: jest.fn(),
    groupEnd: jest.fn(),
    debug   : jest.fn(),
    trace   : jest.fn(),
    group   : jest.fn(),
    log     : jest.fn()
}

/**
 *  Init Test
 */
describe( 'End Tag - Parser', () => {
    const Parser = new ParseEndTag( 
        Lex, 
        TokenList._el_span
    )
    
    it( 'Found End Span Tag', () => {
        Lex.tokenizeThis('</span>')
        expect(Parser.exec()).toBe(true)
        
        Lex.tokenizeThis('< / span >')
        expect(Parser.exec()).toBe(true)

    } )

    it( 'Unfound End Span Tag', () => {
        Lex.tokenizeThis('@')
        expect(Parser.exec()).toBe(false)

        Lex.tokenizeThis('<&')
        expect(Parser.exec()).toBe(false)

        Lex.tokenizeThis('</div')
        expect(Parser.exec()).toBe(false)

        Lex.tokenizeThis('</span&')
        expect(Parser.exec()).toBe(false)
    } )

} )