import   Lex                from '../../Lexer'
import { ParseDataContent } from '../ParseDataContent'

/**
 * Modified the global behavior of the Console.API
 */
global.console = <any>{
    // Overrided
    error: console.log,
    // Disabled
    groupCollapsed: jest.fn(),
    groupEnd: jest.fn(),
    debug   : jest.fn(),
    trace   : jest.fn(),
    group   : jest.fn(),
    log     : console.log
}

/**
 * Init Test
 */
describe( 'Content Parser', () => {
    const content   = '...'
    const lineBreak = '\n'
    const lessSign  = '<'

    it('Capture Content Char Data or Line Break', () => {
        const Parser = new ParseDataContent( Lex )

        expect( Parser.checkLineBreak( lineBreak ) ).toBe( true )

        Lex.tokenizeThis( lessSign )
        expect( Parser.exec() ).toBeFalsy()
        expect( Parser.nodeAST().Content  ).toBeNull()

        Lex.tokenizeThis( lineBreak + lessSign )
        expect( Parser.exec() ).toBeTruthy()
        expect( Parser.nodeAST().Content!.data() ).toEqual('\n')
        
        Lex.tokenizeThis( content + lineBreak + lessSign )
        Parser.exec()
        expect( Parser.nodeAST().Content!.data() ).toEqual( content )
        Parser.exec()
        expect( Parser.nodeAST().Content!.data() ).toEqual( lineBreak )

        Lex.tokenizeThis( content + lessSign )
        Parser.exec()
        expect( Parser.nodeAST().Content!.data() ).toEqual( content )
        
        Lex.tokenizeThis( lineBreak + content + lessSign )
        Parser.exec()
        expect( Parser.nodeAST().Content!.data() ).toEqual( lineBreak )

    })

})