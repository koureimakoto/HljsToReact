import { Lexer }             from '../Lexer'
import { ContentAST }        from '../AST/ContentAST'
import { ParseElement }      from './ParseElement'
import { ParseDataContent }  from './ParseDataContent'
import { CommonsParseTools } from './CommonsParseTools'
import Terminal from '../test/setup'

export class
ParseElementContent extends CommonsParseTools {
    Flag = {
        content: true,
        element: true
    }
   
    AST !: ContentAST | null

    constructor( ExtLex: Lexer ) {
        super( ExtLex )
    }

    exec(): boolean {
        this.register([
            () => this.elementContentRules()
        ])
        return super.exec()
    }

    clear() {
        this.setContent( true )
        this.setElement( true )
    }

    setElement( flag: boolean ) {
        this.Flag.element = flag
        return this.Flag.element
    }

    setContent( flag: boolean ) {
        this.Flag.content = flag
        return this.Flag.content
    }

    checkElement() {
        return this.Flag.element
    }

    checkContent() {
        return this.Flag.content
    }

    checkFlag() {
        return ( this.Flag.content || this.Flag.element )
    }


    elementContentRules() {
        this.AST = new ContentAST( null )

        const DataContentParser: ParseDataContent = new ParseDataContent( this.Lex )
        const ElementParser    : ParseElement     = new ParseElement( this.Lex )

        // DataContent
        if( DataContentParser.exec() ) {
            this.AST.first( DataContentParser.nodeAST() )
        } 
        else {
            this.Lex.saveState()
            if( ElementParser.exec() ) {
                this.Lex.saveState()
                this.AST.first( new ContentAST( ElementParser.nodeAST() ) )                
            }
            else {
                return false
            }
            this.Lex.rollBackState()
        }
        
        this.Lex.saveState()
        while( this.checkFlag() ) {
            // DataContent?*            
            
            if( this.checkContent() )
                while( this.setContent( DataContentParser.exec() ) ) {
                    this.Lex.saveState()
                    this.AST.add( DataContentParser.nodeAST() )
                    this.setElement( true )    
                }
            this.Lex.rollBackState()
            
           
            if( this.checkElement() ) {
                    if( this.setElement( ElementParser.exec() )) {
                        this.Lex.saveState()
                        this.AST.add( new ContentAST( ElementParser.nodeAST() ) )
                        this.setContent( true )
                    }  
                }
            this.Lex.rollBackState()           
        }
        this.clear()
        return true
    }


    printClear() {
        Terminal.log(
        'CONTENT: '+ (this.Flag.content === true ? 't' :  'f') +'\n'+
        'ELEMENT: '+ (this.Flag.element === true ? 't' :  'f') 
        )
    }

    nodeAST(): ContentAST {
        return this.AST!
    }
}