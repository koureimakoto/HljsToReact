import { TokenList }           from '../DataTypes'
import { AttrExprAST }         from '../AST/AttrExprAST'
import { CharDataExprAST }     from '../AST/CharDataExprAST'
import { StartTagExprAST }     from '../AST/StartTagExprAST'
import { CommonsParseElement } from './CommonsParseElement'

/**
 * Start Tag Parser, only to Start Tag Expr AST Elements
 * 
 * @see https://www.w3.org/TR/REC-xml/#NT-STag
 */
export class
ParseStartTag extends CommonsParseElement{
    private readonly doubleQuot: string = '"'
    private readonly equalSign : string = '='
    private buffer      : string = ''
    private attContent  : string = ''
    private attNameToken: number = TokenList._attr_name_class


    exec(): boolean {
        
        this.register([
            () => this.checkStart(),
            () => this.checkName(),
            () => this.checkAttributeName(),
            () => this.checkEqual(),
            () => this.checkDoubleQuot(),
            () => this.checkAttributeValue(),
            /* Check Attribute Value stop in Double Quotes */
            () => this.checkEnd()
        ])

        return super.exec()
    
    }

    nodeAST() {
        return (
            new StartTagExprAST(
                // Span, Div, Body ...
                this.elementNameToken,
                new AttrExprAST( 
                    // Id, Class ...
                    this.attNameToken, 
                    new CharDataExprAST(
                        // " ... " 
                        TokenList._attr_content,
                        this.attContent
                    )
                )
            )
        )
    }


    /**
     * Return the Attribute Value
     * 
     * ```ts
     * attributeValue
     *  string | empty
     * ```
     */
    attributeValue(): string {
        return this.attContent
    }

    /**
     * Search through the content until Double Quotes or MAX_LEN
     * 
     * ```ts
     * checkAttributeValue
     *  void | throw Error
     * ```
     */
    checkAttributeValue(): void {
        this.buffer  = this.Lex.nextChar()
        this.attContent = ''

        // While different of '"' concat character inside content
        while( !this.Lex.endOf( this.buffer, this.doubleQuot ) ) {
            this.attContent += this.buffer
            this.buffer = this.Lex.nextChar()
            this.maxSafeLoop()
        }
        this.minSafeLoop()
        

        // Filter space code.
        this.attContent = this.filter( this.attContent )
    }

    /**
     * Search for Attribute Name
     * 
     * ```ts
     * checkAttributeName
     *  void | throw Error
     * ```
     */
    checkAttributeName() {
        if( this.newToken() !== this.attNameToken )
            throw 'ParseEndTag.checkStart( Token: Attribute Name )'
    }

    /**
     * Search for '='
     * Code U+003D 
     * 
     * ```ts
     * checkEqual
     *  void | throw Error
     * ```
     */
    checkEqual() {
        if( this.newToken() !== this.equalSign.charCodeAt(0) )
            throw 'ParseEndTag.checkStart( token: Equal )'
    }

    /**
     * Search for '"'
     * Code U+0022 
     * 
     * ```ts
     * checkDoubleQuot
     *  void | throw Error
     * ```
     */
    checkDoubleQuot() {
        if( this.newToken() !== this.doubleQuot.charCodeAt(0) )
            throw 'ParseEndTag.checkStart( token: Double Quotes )'
    }

}