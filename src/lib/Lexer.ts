/* eslint-disable @typescript-eslint/no-inferrable-types */
import Log  from './Log'
import { TokenList }        from './DataTypes'

/**
 *  **Span Lexer Class**
 * 
 * For  using to request and response token.  The simple lexical analyzer class. 
 * Return  a few  token types,  but necessary to  implement the  abstract parser 
 * queue, in Span Parser.
 * ---
 * ```typescript
 * CLASS
 *   Log              : InsideConsoleLog // Only verbose Log in terminal
 *   next             : number           // Next char request by parser
 *   identifier       : string           // Span or class identifier
 *   txt              : string           // Buffer text content 
 *   size             : string           // Size of code string, as a EOF.
 *   private last_char: string           // last_char before next
 * ```
 */

export class
Lexer {
    private next      : number = 0
    private size      : number = 0
    private txt       : string = ''
    private last_char : string = ''
    private identifier: string = ''
    private state     : number = this.next

    /**
     * 
     * ```typescript
     * tokenizeThis 
     *   >> txt: string
     * ```
     */
    tokenizeThis(txt: string): void {
        this.txt = txt
        this.size= txt.length
        this.next= 0 
    }

    /**
     * 
     * ```typescript
     * nextChar : getter 
     *   << nextChar
     * ```
     */   
    
    nextChar(): string {
        return this.txt[this.next++]
    }

    /**
     * 
     * ```typescript
     * lastChar 
     *   << last_char
     * ```
     */
    lastChar() : string {
        return this.last_char
    }

    /**
     * ONLY if necessary returns to the previous position
    * ```typescript
     * returnToPrev: getter 
     *   << void
     * ```
     */
    returnToPrev(): void {
        this.next--
        this.last_char = this.txt[this.next]
    }


    unreadTxt() {
        let bufferNext = this.next
        let txt: string = ''

        while( bufferNext < this.size )
            txt += this.txt[ bufferNext++ ]

        return txt
    }


    saveState() {
        this.state = this.next
    }

    rollBackState( ) {
        this.next  = this.state
    }
    
    /**
     * DEPREACATED
     * ```typescript
     * classContent: getter 
     *   << buffer: string
     * ```
     */
    classContent() {
        let buffer  = ''

        this.last_char = this.nextChar()
        while(!this.endOfContent(this.last_char)) { 
            buffer += this.last_char
            this.last_char = this.nextChar()
        }

        this.returnToPrev()
        return buffer
    }


    /**
     * 
     * ```typescript
     * classContent: getter 
     *   << buffer: string
     * ```
     */
     dataContent() {
        let buffer  = ''

        this.last_char = this.nextChar()
        while(!this.endOfContent(this.last_char)) { 
            buffer += this.last_char
            this.last_char = this.nextChar()
        }

        this.returnToPrev()
        return buffer
    }


    /**
     * Return if the code text size ended. As a EOF 
     * ```typescript
     * checkEof 
     *   >> txt: string
     * ```
     */
    checkEof(): boolean {
        if( this.next < this.size ) {
            return false
        }
        return true
    }


    /**
     * Get true if there are no more letter to return in the content   
     * ```typescript
     * endOfContent 
     *   >> letter: string
     *   << boolean
     * ```
     */
    endOfContent( letter : string ) {   
        const quotmark: string = '"'
        const lessthan: string = '<'
        return (
            letter !== lessthan &&
            letter !== quotmark &&
            this.next <= this.size       ?
            false : true
        )

    }

    endOf( letter: string, wrapperContentSign: string ) {
        return (
            letter !== wrapperContentSign &&
            this.next <= this.size        ?
            false: true
        )
    }

    /**
     * The monst important method inside Lexer Class. It is responsible for get
     * the specific token for using in Span Parser.
     * ```typescript
     * token 
     *   << TokenList: number
     * ```
     */
    token(): number {
        Log.initLexer()

        this.last_char = this.nextChar()

        // Whitespace scape
        while( this.space(this.last_char) )
            this.last_char = this.nextChar()

        // [a-zA-Z] only
        if( this.alpha(this.last_char) ) {
            this.identifier = ''
            do {
                this.identifier += this.last_char
                this.last_char  = this.nextChar()
            }while( this.alpha(this.last_char))
            this.next--
            console.log('Identifier return: ' + this.identifier )

            if( this.identifier === 'span' )
                return TokenList._el_span

            if( this.identifier === 'class' )
                return TokenList._attr_name_class
            return TokenList._undef
        }

        // Return itself ASCII code as a token
        if( this.openTagSimbol(this.last_char) ||
            this.closeTagSimbol(this.last_char)||
            this.checkLetter(this.last_char, '"') ||
            this.checkLetter(this.last_char, '=' ) ||
            this.checkLetter(this.last_char, '/' ) )
            return this.last_char.charCodeAt(0)
        return TokenList._undef
    }

    /**
     * Check all whitespace code
     * ```typescript
     * space
     *   >> letter: string 
     *   << boolean
     * ```
     */
    space( letter: string ): boolean {
        const space = 
        letter === ' '
        || letter === '\u00a0'
        || letter === '\u1680'
        || letter === '\u200a'
        || letter === '\u2028'
        || letter === '\u2029'
        || letter === '\ufeff'
        
        letter = space  === true ? 'whitespace' : 
                 letter === ''   ? 'empty'      : letter

        console.log('Letter( ' + letter + ' ).test( whitespace.All )')
        return space ? true : false
    }

    /**
     * Check [a-zA-Z]
     * ```typescript
     * alpha
     *   >> letter: string 
     *   << buffer: boolean
     * ```
     */
    alpha( letter: string ): boolean {
        const buffer = /^[a-zA-Z]$/.test( letter )
        console.log( 'Alpha letter: ' + ( buffer ? 'letter' : 'not found' ) )
        return buffer
    }
    
    /**
     * Compares the inputs
     * ```typescript
     * checkLetter
     *   >> letter    : string
     *   >> check_this: string 
     *   << boolean
     * ```
     */
    checkLetter( letter: string, check_this: string ) {
        console.log( 'Letter( ' + letter + ' ).test( ' + check_this + ' )' )
        return letter === check_this ? true : false
    }


    /**
     * Compares the inputs
     * ```typescript
     * openTagSimbol
     *   >> letter    : string
     *   >> check_this: string 
     *   << boolean
     * ```
     */    
    openTagSimbol( letter: string ) {
        return this.checkLetter( letter, '<' )
    }

    /**
     * Compares the inputs
     * ```typescript
     * closeTagSimbol
     *   >> letter    : string
     *   >> check_this: string 
     *   << boolean
     * ```
     */
    closeTagSimbol( letter: string ) {
        return this.checkLetter( letter, '>' )
    }


}


const Lex = new Lexer()
export default Lex