import hljs from 'highlight.js'
import { CodeGen } from  './CodeGen'
import { dRequest, LogFlagList  } from './DataTypes'
import Lex from './Lexer'
import Parser from './Parser'


/**
 * You are Welcomne the Simple Span Parser Engine developed by Makoto World. 
 * 
 * To get start is very simples. You needo to pass a code using string, or a 
 * function.
 * 
 * If you choose 'string', attention to line breaks using '\n', but there is 
 * another way I think is more simples.
 * 
 * ---
 * FUNCTION
 *   >> code: string      //Code to convert
 *   >> lang: string      //Programing language
 *   >> className: string // Your class
 *   << JSX.Elements
 */
export default function 
HljsToReact( request: dRequest ) {
    const cname = request?.className ? ' ' + request.className : ''

    // Filter the request
    if( request.code === '') 
        return (
            <span className='hljsToReact error-code'>
                Empty Code to convert
            </span>
        )
    if( request.lang === '' )
        return (
            <span className='hljsToReact error-lang'>
                Sorry! I don&apos;t work with auto programing language detection.
            </span>
        )

    const hljsBuffer  = hljs.highlight( request.code, { language: request.lang }).value

    //return <pre>{hljsBuffer}</pre>

    // Create a Lexer Class to tokenize HLJS return
    Lex.tokenizeThis(hljsBuffer)

    // Create a Parser to process all Span token generated from HLJS lib
    const SpanParser  = new Parser(0)

    // Initialize the Parser Engine
    if( SpanParser.exec() === LogFlagList._parser_error )
        return <>Abstract Syntaxy Queue Error : Level 0</>

    const AST =  SpanParser.AST

    const comp = (
        <CodeGen AST= {AST} options={request.options} className={cname}/>
    )

    return comp
}