import { LogFlagList, TokenList, SpanParseNode } from './DataTypes'
import  AbstractSyntaxTree from './AbstractSyntaxTree'
import  Lex              from './Lexer'
import  Log               from './Log'

/**
 *  **Span Parser Class**
 * 
 * The most important class of the application. This parser is very simples, and
 * `only can be process HTML tag span WITH class`. It is using a QUEUE and not a 
 * tree. It means that it does not process tag within tag. Just parse the format
 * returned  by  HIGHLIGHT.JS  lib.  If your code returned fail,  means it has a 
 * syntax error in your code. Until it becomes a Parse Tree Compiler, there will
 * be this problem.  
 * ---
 * Does this mean the module is useless? `NO`. It helps you to prevent the XSS 
 * vunerability attacks. Avoiding using the function `dangerouslySetInnerHTML`.
 *  
 * https://owasp.org/www-community/attacks/xss/
 * 
 * ---
 * ```typescript
 * 
 * CLASS
 *   Scan      : Lexer            // Lexer/HljsToReact Class 
 *   Spanthis.AST   : SpanParserQueue  // Abstract Syntax Queue
 *   Log       : InsideConsoleLog // Only verbose Log in terminal
 *   tagContent: string           // Buffer for tag content
 *   txtContent: string           // Buffer for class text content
 * ```
 */

export default 
class Parser {
    AST : AbstractSyntaxTree
    tagContent: string = ''
    txtContent: string = ''
    lineCount : number = 0


    constructor( level: number ) {
        this.AST = new AbstractSyntaxTree( level )
    }

    /**
     * 
     * ```typescript
     * constructor 
     *   >> Lexer Class
     * ```
     */

    /**
     * The main function of the Parser. It contains the logic to detect Span tag
     * with className(class).
     * 
     * **SYNTAX**
     * 
     * ```typescript
     * CLASS_TEXT
     *    'class' '=' '\"' all text in '\"'
     * CONTENT
     *    '>' all text in '<'
     * 
     * OPEN_SPAN
     *    '<' 'span' CLASS_TEXT '>'
     * 
     * CLOSE_SPAN
     *    '<' '/' 'span' '>'
     * 
     * NEWTAG
     *    CONTENT OPEN_SPAN (NEWTAG | CLOSE_SPAN) CONTENT
     * ```
     */
    exec(): LogFlagList._ok | LogFlagList._parser_error{
        let status  = 0
        Log.initSimpleCompiler()
        
        while( !Lex.checkEof() ) {     
            
            this.saveContent()
            if( this.tagContent !== '') {
                this.AST.add( new SpanParseNode(
                    TokenList._el_content,
                    this.tagContent,
                    )
                )   
            }

        
            
            status = this.openingSpan()
            if( status === LogFlagList._parser_error || 
                status === LogFlagList._Lexer_error ) {
                console.log( 'Parser Failed in ::' )
                console.log( '--- openingSpan( ) ')
                return LogFlagList._parser_error
            }

            this.saveContent()
            status = this.closingSpan()
            if( status === LogFlagList._parser_error || 
                status === LogFlagList._Lexer_error ) {
                console.log( 'Parser Failed in ::' )
                console.log( '--- closingSpan( ) ')
                return LogFlagList._parser_error
            }

            this.AST.add( new SpanParseNode(
                TokenList._el_span,
                this.tagContent,
                this.txtContent,
                )
            )

            this.clearBuffer()
            this.saveContent()

            if( this.tagContent !== '') {
                this.AST.add( new SpanParseNode(
                    TokenList._el_content,
                    this.tagContent,
                    )
                )   
            }

            if( this.AST.checkLevel()) {
                return LogFlagList._ok
            }
        } 

    

        Log.endSimpleCompiler()

        return LogFlagList._ok
    }


 

    /**
     * Receive a string HTML entities and convert to plain/text
     * ```typescript
     * filter
     *   >> txt         : string 
     *   << filterd_txt : string
     * ```
     */
    filter( txt: string ) : string {
        return   txt.replace(/&lt;/g   , '<' )
                    .replace(/&gt;/g   , '>' )
                    .replace(/&amp;/g  , '&' )
                    .replace(/&#39;/g  , '\'') 
                    .replace(/&#x27;/g , '\'') 
                    .replace(/&quot;/g , '"' )  
    }


    lines( ): number {
        return this.lineCount
    }

    /**
     * Save de Class Text Content or Tag Text Content, it can also detect \n 
     * ```typescript
     * saveContent 
     *   << void
     * ```
     */
    saveContent(): void {
        let buffer = Lex.nextChar()
        let   content : string = ''
        
        while( !Lex.endOfContent(buffer) ) {
 
            if( buffer === '\n') {

                if(this.txtContent !== '') {
                    this.AST.add(
                        new SpanParseNode(
                            TokenList._el_span,
                            this.filter(content),
                            this.txtContent,
                        )
                    )
                       
                } 
                else {
                    this.AST.add( 
                        new SpanParseNode(
                            TokenList._el_content,
                            this.filter(content),
                        )
                    )
                }
                this.AST.add( 
                    new SpanParseNode(
                        TokenList._linebreak,
                        ' ',
                    )
                )   
                buffer = ''
                content= ''
                this.lineCount++
                
            }
            
            content += buffer
            buffer = Lex.nextChar()
        }
        Lex.returnToPrev()
        this.tagContent = this.filter( content )
    }


    // Will be removed (deprecated)
    perror(msg: string, error_type: number) {
        if( error_type === LogFlagList._Lexer_error )
            console.log('Token Error::')
        else
        if( error_type === LogFlagList._parser_error )
            console.log('Parser Error')
        console.log('- - - ' +msg)
    }

    /**
     * Only clean the buffer content for each new child
     */
    clearBuffer(): void {
        this.tagContent = ''
        this.txtContent = ''
    }
 
    /**
     * Search for the Opening Span Tag 
     * `< span class=" txtContent " >`
     * 
     * ```typescript
     * openingSpan
     *   << LogFlagList.(_ok | _error)
     * ```
     */
    openingSpan(): LogFlagList._ok | LogFlagList._Lexer_error {
        Log.initParser('Opening Span Tag')

        // IF token '<' 
        if(Lex.token() !== '<'.charCodeAt(0)){
            this.perror( 
                'Undefined Token (' + Lex.lastChar() + ')',
                LogFlagList._Lexer_error 
            )

            return LogFlagList._Lexer_error
        }        
        Log.endLexer()
        
        let  last_token = Lex.token() 
        // IF token 'span'
        if( last_token !== TokenList._el_span ) {
            this.perror( 
                'Undefined Token (' + Lex.lastChar() + ')',
                LogFlagList._Lexer_error 
            )
            return LogFlagList._Lexer_error
        }
        Log.endLexer()

        // IF CLASS_TEXT
        last_token = Lex.token()
          // The format is prepared to use empty class span, but not yet used
        if(  last_token    === TokenList._attr_name_class ) {
            if( Lex.token() !== '='.charCodeAt(0) ) {
                this.perror( 
                    'Undefined Token (' + Lex.lastChar() + ')',
                    LogFlagList._Lexer_error 
                )
                return LogFlagList._Lexer_error
            }
            Log.endLexer()

            if( Lex.token() !== '"'.charCodeAt(0) ) {
                this.perror( 
                    'Undefined Token (' + Lex.lastChar() + ')',
                    LogFlagList._Lexer_error 
                )
                return LogFlagList._Lexer_error
            }
            Log.endLexer()

            // Get the text contente inside '\"' 
            this.txtContent = Lex.classContent()

            // If token '\"'
            if( Lex.token() !== '"'.charCodeAt(0) ) {
                this.perror( 
                    'Undefined Token (' + Lex.lastChar() + ')',
                    LogFlagList._Lexer_error 
                )
                return LogFlagList._Lexer_error
            }
            Log.endLexer()
        
            // Token Buffer to use with not empty class span only
            last_token = Lex.token()
        }
        Log.endLexer()
        // IF token '>' : closing correctly
        if( last_token !== '>'.charCodeAt(0) ) {
            this.perror( 
                'Undefined Token (' + Lex.lastChar() + ')',
                LogFlagList._Lexer_error 
            )
            return LogFlagList._Lexer_error
        }   
        Log.endLexer()
        Log.endParser({ name: 'Opening Span Tag' })

        return LogFlagList._ok
    }

    /**
     * Search for the Closing Span Tag 
     * `< / span >`
     * 
     * ```typescript
     * closingSpan
     *   << LogFlagList.(_ok | _error)
     * ```
     */
    closingSpan(): LogFlagList._ok | LogFlagList._Lexer_error {
        Log.initParser('Closing Span Tag')

        //IF token '<'
        if(Lex.token() !== '<'.charCodeAt(0)){
            this.perror( 
                'Undefined Token (' + Lex.lastChar() + ')',
                LogFlagList._Lexer_error 
            )
            return LogFlagList._Lexer_error
        }
        Log.endLexer()

        let last_token = Lex.token()
        //IF token '/'
        if( last_token !== '/'.charCodeAt(0)){
            console.log("GLUBGLUB")

            // Recursive operation to enter in tree
            let newParser: Parser | null = new Parser( this.AST.downLevel() )
            
            // Only return the leaf or child if the next node is a OpenningSpan
            if( !newParser.exec() ) {
                console.log("Error to Down on the Tree")
                return LogFlagList._Lexer_error
            }

            // Add a new AST node
            const node = new SpanParseNode(
                TokenList._exp,
                '', '',
                newParser.AST
            )

            newParser = null
            this.AST.add( node )
            console.log("Ending AST dive")
            Log.endLexer()
            return LogFlagList._ok
        }
        Log.endLexer()

        //IF token 'span'
        if( Lex.token() !== TokenList._el_span ) {
            this.perror( 
                'Undefined Token (' + Lex.lastChar() + ')',
                LogFlagList._Lexer_error 
            )
            return LogFlagList._Lexer_error
        }
        Log.endLexer()

        //IF token '>' : closing correctly
        if( Lex.token() !== '>'.charCodeAt(0) ) {
            this.perror( 
                'Undefined Token (' + Lex.lastChar() + ')',
                LogFlagList._Lexer_error 
            )
            return LogFlagList._Lexer_error
        } 
        Log.endLexer()
        Log.endParser({ name: 'Closing Span Tag' })

        return LogFlagList._ok
    }
}